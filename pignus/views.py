from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from .models import Profile, Orden
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm, User
from django.contrib.auth.models import User 
from django.contrib.auth import authenticate, logout, login as auth_login
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView
import datetime
import json

######## cargar templates ###########

def index(request):
    user = request.session.get('user', None)
    return render(request,'home.html')

def somos(request):
    return render(request,'somos.html')

def contactenos(request):
    return render(request,'contactenos.html')    
    
def ordenf(request):
    return redirect(request, 'profile.html')

def admin(request):
    context = { 
        'ordenes': Orden.objects.all()
    }
    return render(request, 'admin.html', context)
##############################################

#### funciones #####
class PostCreateView(LoginRequiredMixin, CreateView):
    model = Orden
    fields = ['titulo','contenido', 'imagen']
    def form_valid(self, form):
        form.instance.autor = self.request.user
        return super().form_valid(form) 

class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Orden
    fields = ['titulo','contenido', 'imagen']
    def form_valid(self, form):
        form.instance.autor = self.request.user
        return super().form_valid(form) 
    
    def test_func(self):
        orden = self.get_object()
        if self.request.user == orden.autor:
            return True
        return False

class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Orden
    success_url = '/'
    context_object_name = 'orden'
    def test_func(self , *args, **kwargs):
        orden = self.get_object()
        if self.request.user == orden.autor or self.request.user.is_staff:
            return True
        return False


class PostListView(ListView):
    model = Orden
    template_name = 'admin.html'
    context_object_name = 'ordenes'
    ordering = ['-fecha']

class PostDetailView(DetailView):
    model = Orden
    context_object_name = 'orden'

def edit(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Datos actualizados')
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form   
    }
    return render(request, 'edit.html', context )


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Cuenta creada')
            return redirect('login')
    else:
        form = UserRegisterForm()

    return render(request, 'registro.html', {'form':form})        

def logout(request):
    del request.session['user']
    return redirect('index')

def login_iniciar(request):
    username = request.POST.get('username','')
    password = request.POST.get('password','')
    User = authenticate(request, username = username, password = password)
    print(User)
    if User is not None:
        auth_login(request, User)
        return redirect('index')
    else:
        return HttpResponse('Usuario no existe, registrate!')

def login(request):
    User = request.session.get('User', None)
    return render(request, 'login.html',{})

@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Datos actualizados')
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form,  
    }

    return render(request, 'profile.html', context)